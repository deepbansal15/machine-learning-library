#ifndef __DEFINES_H__
#define __DEFINES_H__

#ifdef CNN_NAMESAPCE
#define CNN_NAMESPACE_BEGIN namespace cnn{
#define CNN_NAMESPACE_END }
#define USE_CNN_NAMESPACE using namespace cnn;
#else
#define CNN_NAMESPACE_BEGIN
#define CNN_NAMESPACE_END
#define USE_CNN_NAMESPACE
#endif

#define DEFAULT_ALIGNMENT 64

#ifndef align_up
/// 4 (100), 3(11), ~3(..1100) 
/// To get lower or same then direct AND otherwise add 3 to get next higher divisible number
#define align_up(num, align) \
    (((num) + ((align) - 1)) & ~((align) - 1))
#endif

#endif
