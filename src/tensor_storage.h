#ifndef __TENSOR_STORAGE_H__
#define __TENSOR_STORAGE_H__

#include "defines.h"

/// Main idea behind tensor storage is to keep all the tensor in a single memory area
/// with the remaining code only using TensorHandles
/// Incomplete code

/// TODO create a custom hash
#include "unordered_map"

CNN_NAMESPACE_BEGIN

/******
 * Manages tensor allocation and destruction and also
 * controls where the tensor are allocated
******/

struct Tensor;
struct TensorDescriptor;

enum TensorStorageLoc
{
    CPU,
    GPU
};

struct TensorStorage
{
    // std::unordered_map<int,size_t> tensors;
    // char* cpu_memory;
    int64_t free_loc;
};

namespace tensor_storage
{
    TensorStorage create_storage(int bytes_size);
    void destroy_storage(TensorStorage* storage);
    Tensor* create_tensor(TensorStorage& storage, TensorDescriptor* descriptor,TensorStorageLoc loc);
    void destroy_tensor(TensorStorage& storage,Tensor* tensor);
    bool change_mem_loc(TensorStorage& storage,Tensor* tensor, TensorStorageLoc loc);
}

CNN_NAMESPACE_END

#endif