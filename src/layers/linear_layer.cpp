#include "linear_layer.h"

#include "../tensor_storage.h"
#include "../tensor.h"

#include <cstring>

CNN_NAMESPACE_BEGIN

namespace linear_layer
{
    LinearLayer* create_linear_layer(int batch_size,int n_inputs, int n_outputs)
    {
        LinearLayer* layer = new LinearLayer();
        TensorStorage storage = tensor_storage::create_storage(0);
        
        TensorDescriptor descriptor {
            .batch_size = 1,
            .channels = 1,
            .width = n_inputs * n_outputs,
            .height = 1,
            .element_size = sizeof(float),
            .data_type =  TensorDataType::FLOAT
        };

        Tensor* weights = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);
        Tensor* gradients = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);
    
        descriptor.width = n_outputs;
        Tensor* bias = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);
        descriptor.batch_size = batch_size;
        Tensor* output = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);

        layer->num_inputs = n_inputs;
        layer->num_outputs = n_outputs;
        layer->batch_size = batch_size;

        layer->h_bias = bias;
        layer->h_output = output;
        layer->h_weights = weights;
        layer->h_gradient = gradients;

        return layer;
    }

    void forward_pass(LinearLayer& layer,const Tensor& input)
    {
        int output_width = layer.num_outputs;
        int input_width = layer.num_inputs;

        float* weights = reinterpret_cast<float*>(layer.h_weights->data);
        float* inputs = reinterpret_cast<float*>(input.data);
        float* outputs = reinterpret_cast<float*>(layer.h_output->data);
        float* bias = reinterpret_cast<float*>(layer.h_bias->data);

        for(int b=0;b<layer.batch_size;b++)
        {
            int offset_b = b * output_width;
            for(int i=0;i<output_width;i++)
            {
                int o_index = offset_b + i;
                outputs[o_index] = bias[i];
                int offset_w = i * input_width;
                for(int j=0;j<input_width;j++)
                {
                    outputs[o_index] += weights[offset_w + j] * inputs[j];
                }
            }
        }
    }
    
    void update(LinearLayer& layer,const Tensor& input, const Tensor& delta)
    {
        TensorStorage storage = tensor_storage::create_storage(0);
        Tensor* delta_weights = tensor_storage::create_tensor(storage,&layer.h_weights->descriptor,TensorStorageLoc::CPU);
        Tensor* delta_bais = tensor_storage::create_tensor(storage,&layer.h_bias->descriptor,TensorStorageLoc::CPU);

        int input_width = layer.num_inputs;
        int output_width = layer.num_outputs;
        
        float* d = reinterpret_cast<float*>(delta.data);
        float* inputs = reinterpret_cast<float*>(input.data);
        float* d_weights = reinterpret_cast<float*>(delta_weights->data);
        float* d_bias = reinterpret_cast<float*>(delta_bais->data);

        memset(d_weights,0,delta_weights->descriptor.get_byte_size());
        memset(d_bias,0,delta_bais->descriptor.get_byte_size());
        
        for(int b=0;b<layer.batch_size;b++)
        {
            int offset_b = b * input_width;
            for(int i=0;i<output_width;i++)
            {
                d_bias[i] += d[i];
                int offset_w = i * input_width;
                for(int j=0;j<input_width;j++)
                {
                    d_weights[offset_w + j] += inputs[offset_b + j] * d[i];
                }
            }
        }
        
        /// Update weights and bias
        float* weights = reinterpret_cast<float*>(layer.h_weights->data);
        float* bias = reinterpret_cast<float*>(layer.h_bias->data);

        int total_weights = input_width * output_width;
        for(int i=0;i<total_weights;i++)
        {
            // d_bias[i] += d[i];
            weights[i] -= d_weights[i];
        }

        for(int i=0;i<output_width;i++)
        {
            bias[i] -= d_bias[i];
        }
    }
    
    Tensor* backward_pass(LinearLayer& layer,const Tensor& prev_layer_delta)
    {
        TensorStorage storage = tensor_storage::create_storage(0);
        TensorDescriptor descriptor {
            .batch_size = 1,
            .channels = 1,
            .width = layer.num_inputs,
            .height = 1,
            .element_size = sizeof(float),
            .data_type =  TensorDataType::FLOAT
        };

        Tensor* delta = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);

        int input_width = layer.num_inputs;
        int output_width = layer.num_outputs;
        
        float* d = reinterpret_cast<float*>(delta->data);
        float* p_d = reinterpret_cast<float*>(prev_layer_delta.data);
        float* weights = reinterpret_cast<float*>(layer.h_weights->data);

        memset(d,0,delta->descriptor.get_byte_size());
        
        for(int i=0;i<output_width;i++)
        {
            int offset_w = i * input_width;
            for(int j=0;j<input_width;j++)
            {
                d[j] += weights[offset_w + j] * p_d[i];
            }
        }

        return delta;
    }
}

CNN_NAMESPACE_END