#ifndef __LINEAR_LAYER_H__
#define __LINEAR_LAYER_H__

#include "../defines.h"

CNN_NAMESPACE_BEGIN

struct Tensor;

struct LinearLayer
{
    int num_inputs;
    int num_outputs;
    int channels;
    int batch_size;

    Tensor* h_weights;
    Tensor* h_bias;
    Tensor* h_output;
    Tensor* h_gradient;
};

struct BiLinearLayer
{
    /// TODO - Optional
};

namespace linear_layer
{
    LinearLayer* create_linear_layer(int batch_size,int n_inputs, int n_outputs);
    void forward_pass(LinearLayer& layer,const Tensor& input);
    void update(LinearLayer& layer,const Tensor& input, const Tensor& delta);
    Tensor* backward_pass(LinearLayer& layer,const Tensor& prev_layer_delta);
}

CNN_NAMESPACE_END

#endif