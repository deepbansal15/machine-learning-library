#ifndef __TEST_LINEAR_LAYER_H__
#define __TEST_LINEAR_LAYER_H__

namespace test_linear_layer
{
    bool forward();
    bool backward_update();
}

#endif