#include "test_linear_layer.h"

#include "../layers/linear_layer.h"

#include "../defines.h"
#include "../tensor.h"
#include "../tensor_storage.h"

#include <iostream>

USE_CNN_NAMESPACE

namespace test_linear_layer
{
    LinearLayer* default_setup(int n_batch,int n_inputs,int n_outputs)
    {
        LinearLayer* layer = linear_layer::create_linear_layer(n_batch,n_inputs,n_outputs);
        
        float* weights = reinterpret_cast<float*>(layer->h_weights->data);
        float* bias = reinterpret_cast<float*>(layer->h_bias->data);
        /// Setup weights
        for(int i=0; i<n_outputs; i++)
        {
            bias[i] = 1;
            int w_offset = i * n_inputs;
            for(int j=0; j<n_inputs; j++)
            {
                weights[w_offset + j] = 1;
            }
        }

        return layer;
    }
    /// TODO dimension comparison
    bool forward()
    {
        int n_inputs = 10;
        int n_outputs = 5;
        int n_batch = 2;

        TensorDescriptor descriptor {
            .batch_size = n_batch,
            .channels = 1,
            .width = n_inputs,
            .height = 1,
            .element_size = sizeof(float),
            .data_type =  TensorDataType::FLOAT
        };

        TensorStorage storage = tensor_storage::create_storage(0);
        Tensor* input = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);

        float* in = reinterpret_cast<float*>(input->data);
        for(int i=0;i<n_batch*n_inputs;i++)
        {
            in[i] = 1;
        }

        LinearLayer* layer = default_setup(n_batch,n_inputs,n_outputs);
        linear_layer::forward_pass(*layer,*input);

        float* out = reinterpret_cast<float*>(layer->h_output->data);
        for(int i=0;i<n_outputs*n_batch;i++)
        {
            if(out[i] != n_inputs + 1) return false;
        }

        return true;
    }

    bool backward_update()
    {
        int n_inputs = 10;
        int n_outputs = 5;
        int n_batch = 2;

        TensorDescriptor descriptor {
            .batch_size = 1,
            .channels = 1,
            .width = n_outputs,
            .height = 1,
            .element_size = sizeof(float),
            .data_type =  TensorDataType::FLOAT
        };

        TensorStorage storage = tensor_storage::create_storage(0);
        Tensor* delta = tensor_storage::create_tensor(storage,&descriptor,TensorStorageLoc::CPU);
        float* d = reinterpret_cast<float*>(delta->data);
        for(int i=0;i<n_inputs;i++)
        {
            d[i] = 1;
        }

        LinearLayer* layer = default_setup(n_batch,n_inputs,n_outputs);
        Tensor* gradient = linear_layer::backward_pass(*layer,*delta);

        float* g = reinterpret_cast<float*>(gradient->data);
        for(int i=0;i<n_inputs;i++)
        {
            std::cout << g[i] << std::endl;
        }

        return false;
    }
}

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include <unistd.h>

using namespace test_linear_layer;

TEST_CASE("sleep a second") {
    CHECK(test_linear_layer::forward() == true);
    CHECK(test_linear_layer::backward_update() == true);
}

