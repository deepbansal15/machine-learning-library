#include "block_allocator.h"

// 
// #include <cstddef>
#include <cassert>

CNN_NAMESPACE_BEGIN

namespace block_allocator
{
    void* alloc(uint64_t size,int align)
    {
        /// Checking if align is power of 2
        assert((align & (align - 1)) == 0);
        /// Checking for size == 0 and align == 0
        if(align && size)
        {
            size += sizeof(uint16_t) + (align - 1);
            void* ptr = malloc(size);
            if(ptr)
            {
                uintptr_t data = align_up(((uintptr_t)ptr + sizeof(uint16_t)), align);
                *((uint16_t *)data - 1) = (uint16_t)(data - (uintptr_t)ptr);
                return (void*)data;
            }
        }

        return nullptr;
    }

    void dealloc(void* data)
    {
        void* ptr = (void*)((uintptr_t)data - (*((uint16_t *)data - 1)));
        free(ptr);
    }
}

CNN_NAMESPACE_END