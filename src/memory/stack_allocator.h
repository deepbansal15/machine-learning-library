#ifndef __STACK_ALLOCATOR_H__
#define __STACK_ALLOCATOR_H__

#include "../defines.h"

#include <stdint.h>

CNN_NAMESPACE_BEGIN

struct StackAllocator
{
    uint64_t allocated_size;
    uint64_t offset;
    void* data;
};

namespace stack_allocator
{
    StackAllocator init(uint64_t size);
    void destroy(StackAllocator* allocator);
    void* alloc(uint64_t size,int alignment);
    void dealloc(void*);
}

CNN_NAMESPACE_END

#endif