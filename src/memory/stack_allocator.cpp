#include "stack_allocator.h"
#include "block_allocator.h"

CNN_NAMESPACE_BEGIN

namespace stack_allocator
{
    StackAllocator init(uint64_t size)
    {
        void* data = block_allocator::alloc(size,DEFAULT_ALIGNMENT);
        
        StackAllocator allocator {
            .data = data,
            .offset = 0,
            .allocated_size = size
        };

        return allocator;
    }
    
    void destroy(StackAllocator* allocator)
    {
        block_allocator::dealloc(allocator->data);
    }

    void* alloc(uint64_t size,int alignment = DEFAULT_ALIGNMENT)
    {
        void* data = block_allocator::alloc(size,alignment);
        return data;
    }

    void dealloc(void* data)
    {
        block_allocator::dealloc(data);
    }
}

CNN_NAMESPACE_END