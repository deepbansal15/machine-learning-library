#ifndef __BUDDY_H__
#define __BUDDY_H__

#include "../defines.h"

CNN_NAMESPACE_BEGIN

struct BuddyAllocator;

namespace buddy_allocator
{
    BuddyAllocator* create(int size);
    void destroy(BuddyAllocator* self);
    int allocmem(BuddyAllocator* self, int size);
    void freemem(BuddyAllocator* self, int offset);
    int size(BuddyAllocator* self, int offset);
    void dump(BuddyAllocator* self);
}

CNN_NAMESPACE_END

#endif//__BUDDY2_H__
