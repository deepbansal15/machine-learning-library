#ifndef __BLOCK_ALLOCATOR_H__
#define __BLOCK_ALLOCATOR_H__

#include "../defines.h"

#include <stdint.h>

CNN_NAMESPACE_BEGIN

namespace block_allocator
{
    void* alloc(uint64_t size,int align = DEFAULT_ALIGNMENT);
    void dealloc(void*);
}

CNN_NAMESPACE_END

#endif