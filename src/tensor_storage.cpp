#include "tensor_storage.h"
#include "tensor.h"
#include "memory/block_allocator.h"

#include <cassert>

CNN_NAMESPACE_BEGIN

namespace tensor_storage
{
    TensorStorage create_storage(int bytes_size)
    {
        return {bytes_size};
    }

    void destroy_storage(TensorStorage* storage)
    {

    }
    
    Tensor* create_tensor(TensorStorage& storage, TensorDescriptor* descriptor,TensorStorageLoc loc)
    {
        /// Default alignment for tensor struct storage
        assert(sizeof(Tensor) <= DEFAULT_ALIGNMENT);
        int64_t total_size = DEFAULT_ALIGNMENT + descriptor->get_byte_size();
        void* ptr = block_allocator::alloc(total_size);
		
        Tensor* t = new (ptr) Tensor();
        t->descriptor = *descriptor;
		t->data = (char*)ptr + DEFAULT_ALIGNMENT;
        return t;
    }

    void destroy_tensor(TensorStorage& storage,Tensor* tensor)
    {
        void* ptr = (void*)tensor;
        block_allocator::dealloc(ptr);
    }

    bool change_mem_loc(TensorStorage& storage,Tensor* tensor, TensorStorageLoc loc)
    {
        return false;
    }
}

CNN_NAMESPACE_END