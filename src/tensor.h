#ifndef __TENSOR_H__
#define __TENSOR_H__

#include "defines.h"
#include <stdint.h>

CNN_NAMESPACE_BEGIN

/// Everything operate on tensors

enum TensorDataType
{
    INT8 = 1,
    INT16 = 2,
    INT32 = 3,
    FLOAT = 4,
    DOUBLE = 5
};



struct TensorDescriptor
{
    int batch_size;
    int channels;
    int height;
    int width;
    int element_size;
    TensorDataType data_type;

    int64_t get_byte_size() const
    { 
        return batch_size * channels * 
               height * width * 
               element_size;
    }
};

struct Tensor
{
    TensorDescriptor descriptor;
    void* data;
};

CNN_NAMESPACE_END

#endif