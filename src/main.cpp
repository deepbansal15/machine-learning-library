#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include "tensor.h"
#include <cstring>

#include "memory/block_allocator.h"
#include "tensor_storage.h"

#include <thread>

// USE_CNN_NAMESPACE

/// Alignment upto AVX 512 bit

/****
 * Everything is a layer
 * Vision layer
 * calculation are done in batches
 * Everybatch gradient calculation are separate
 * The whole CNN gives a nice option to optimize using threads (CPU or GPU)
 * Don't know yet about RNN,LSTM & GRU
 * 
 *****/

/******
 * reinterpret_cast, static_cast & const_cast are
 * compile-time operations
 * dynamic_cast is a runtime opeartion
******/

int main(int argc,char* argv[])
{
    size_t tensor_size = sizeof(cnn::Tensor);
    // size_t int_size = sizeof(int);
    printf("Tensor size %zu \n",tensor_size);

    cnn::TensorStorage storage = cnn::tensor_storage::create_storage(0);
    cnn::TensorDescriptor descriptor {
        .batch_size = 1,
        .channels = 1,
        .width = 1024 * 64,
        .height = 1,
        .element_size = sizeof(float),
        .data_type =  cnn::TensorDataType::FLOAT
    };

    cnn::Tensor* weights = cnn::tensor_storage::create_tensor(storage,&descriptor,cnn::TensorStorageLoc::CPU);

    void* ptr = cnn::block_allocator::alloc(65);
    cnn::block_allocator::dealloc(ptr);

    return 1;
}